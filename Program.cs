﻿using System;
using System.Net.Http;
using System.Text;
using Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;


namespace venuemicroservice
{
    class Program
    {
        static void Main(string[] args)
        {
            var HostName = "dropletrabbit";
            var factory = new ConnectionFactory(){HostName=HostName};
            
            factory.Uri = new Uri(Environment.GetEnvironmentVariable("RABBIT_URI"), UriKind.Absolute);
            

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var ended = new ManualResetEventSlim();
                    channel.QueueDeclare("ven_queue",false,false,false,null);
                    channel.QueueBind("ven_queue", "microservice_ex", "Venue");
                    var consumer = new EventingBasicConsumer(channel);
                    
                    consumer.Received += async (model,ea) => {
                        var message = Encoding.UTF8.GetString(ea.Body);
                        System.Console.WriteLine("---------------------------------------------------------------");
                        System.Console.WriteLine("Venue MS RECEIVED : " + message);
                        
                        var client = new HttpClient();
                        var data = new StringContent(message, Encoding.UTF8, "application/json");
                        var response = await client.PostAsync(Environment.GetEnvironmentVariable("VENUE_ENDPOINT")+"/booking", data);

                        var respmessage = response.Content.ReadAsStringAsync().Result;
                        
                        var respmodel = JsonConvert.DeserializeObject<VendorRespModel>(respmessage);

                        var msreply = new MSReplyModel(){eventid=respmodel.eventid, description=respmodel.description,additional_info=respmodel.additional_info, status=respmodel.status, type="venue", request_type=respmodel.request_type};

                        //sending reply from microservice to aggregator
                        channel.BasicPublish("msres_ex", "", body: Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msreply)));
                        System.Console.WriteLine("---------------------------------------------------------------");
                        System.Console.WriteLine("Venue MS SENDING : " + JsonConvert.SerializeObject(msreply));

                    };
                    
                    channel.BasicConsume("ven_queue", true, consumer);

                    System.Console.WriteLine("Venue microservice running ....");
                    ended.Wait();
                    
                }
            
            }

        }
    }
}