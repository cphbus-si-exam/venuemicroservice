namespace Models
{
    public class MSReplyModel
    {
        public string eventid { get; set; }
        public string description { get; set; }
        public bool status { get; set; }
        public string type { get; set; }
        public string additional_info { get; set; }
        public string request_type { get; set; }
    }
}