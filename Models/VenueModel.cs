namespace Models
{
    public class VenueModel
    {
        public int numberofpersons { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string address { get; set; }
        public string request_type { get; set; }
    }
}